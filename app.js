// app.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express'); 		// call express
var app        = express(); 				// define our app using express
var bodyParser = require('body-parser');
var path = require('path');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//configure views
app.set('view engine','ejs');
app.set('views',path.join(__dirname, 'views'));

//import styles folder
app.use(express.static(__dirname + '/public'));




// set our port
var port = process.env.PORT || 8000;

// connect to our database
var mongoose = require('mongoose');
mongoose.connect(process.env.MONGOLAB_URI, function(err) {
    if(err) {
        console.log('Failed to connect to MongoDB. (Maybe \'mongod\' command is not running?)');
        throw err;
    }
    console.log('Connection to MongoDB established.');
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api

//Import Main Routes
var routes = require('./routes/index');
app.use('/', routes);

//Import Product Routes
var productRoutes = require('./routes/productRouter');
app.use('/api/products', productRoutes);

//Import User Routes
var userRoutes = require('./routes/userRouter');
app.use('/api/users', userRoutes);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('The API is running on port ' + port);