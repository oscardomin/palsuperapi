// app/models/productModel.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var categories = 'otros lácteos pescado carne dulces'.split(' ');

var ProductSchema   = new Schema({
    _id: String,
    itemname: String,
    alias: String,
    description: String,
    avg_price: Number,
    enum: categories
});

module.exports = mongoose.model('Product', ProductSchema);