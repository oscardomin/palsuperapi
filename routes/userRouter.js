//User Page ROUTE
var express = require('express');
var router = express.Router();
var User = require('../models/userModel');

// Routes that end in api/users
// ----------------------------------------------------
router.route('/')

    // create a user (accessed at POST http://localhost:$PORT/api/users)
    .post(function(req, res) {

        var user = new User(); 		                // create a new instance of the User model
        user._id = req.body._id;                         // set the user barcode/number (comes from the request)
        user.password = req.body.password;               // set the products name (comes from the request)
        user.mail = req.body.mail;                     // set the products alias (comes from the request)

        // save the user and check for errors
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });

    })

    // get all the users (accessed at GET http://localhost:$PORT/api/users)
    .get(function(req, res) {
        User.find(function(err, user) {
            if (err)
                res.send(err);

            res.json(user);
        });
    });

// on routes that end in /users/:product_id
// ----------------------------------------------------
router.route('/:product_id')

    // get the product with that barcode number (accessed at GET http://localhost:$PORT/api/users/:user_id)
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    })

    // update the user with this id (accessed at PUT http://localhost:$PORT/api/users/:user_id)
    .put(function(req, res) {

        // use our product model to find the product we want
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);

            if (req.body._id) user._id = req.body._id;                                // update the users barcode
            if (req.body.itemname) user.itemname = req.body.itemname; 	            // update the users name
            if (req.body.alias) user.alias = req.body.alias; 	                        // update the users alias
            if (req.body.description) user.description = req.body.description; 	    // update the users description
            if (req.body.avg_price) user.avg_price = req.body.avg_price;      	    // update the users avg_price
            if (req.body.enum) user.enum = req.body.enum;      	    // update the users avg_price

            // save the product
            user.save(function (err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });
        });
    })

    // delete the user with this id (accessed at DELETE http://localhost:$PORT/api/users/:user_id)
    .delete(function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

module.exports = router;
