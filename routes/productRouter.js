//Product Page ROUTE
var express = require('express');
var router = express.Router();
var Product = require('../models/productModel');

// Routes that end in api/products
// ----------------------------------------------------
router.route('/')

    // create a product (accessed at POST http://localhost:$PORT/api/product)
    .post(function(req, res) {

        var product = new Product(); 		                // create a new instance of the Product model
        product._id = req.body._id;                         // set the products barcode/number (comes from the request)
        product.itemname = req.body.itemname;               // set the products name (comes from the request)
        product.alias = req.body.alias;                     // set the products alias (comes from the request)
        product.description = req.body.description;         // set the products description (comes from the request)
        product.avg_price = req.body.avg_price;             // set the products avg_price (comes from the request)
        product.enum = req.body.enum;                       // set the products category (comes from the request)

        // save the product and check for errors
        product.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Product created!' });
        });

    })

    // get all the products (accessed at GET http://localhost:$PORT/api/products)
    .get(function(req, res) {
        Product.find(function(err, products) {
            if (err)
                res.send(err);

            res.json(products);
        });
    });

// on routes that end in /products/:product_id
// ----------------------------------------------------
router.route('/:product_id')

    // get the product with that barcode number (accessed at GET http://localhost:$PORT/api/products/:product_id)
    .get(function(req, res) {
        Product.findById(req.params.product_id, function(err, product) {
            if (err)
                res.send(err);
            res.json(product);
        });
    })

    // update the product with this id (accessed at PUT http://localhost:$PORT/api/products/:product_id)
    .put(function(req, res) {

        // use our product model to find the product we want
        Product.findById(req.params.product_id, function(err, product) {
            if (err)
                res.send(err);

            if (req.body._id) product._id = req.body._id;                                // update the products barcode
            if (req.body.itemname) product.itemname = req.body.itemname; 	            // update the products name
            if (req.body.alias) product.alias = req.body.alias; 	                        // update the products alias
            if (req.body.description) product.description = req.body.description; 	    // update the products description
            if (req.body.avg_price) product.avg_price = req.body.avg_price;      	    // update the products avg_price
            if (req.body.enum) product.enum = req.body.enum;      	    // update the products avg_price

            // save the product
            product.save(function (err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Product updated!' });
            });
        });
    })

    // delete the product with this id (accessed at DELETE http://localhost:$PORT/api/products/:product_id)
    .delete(function(req, res) {
        Product.remove({
            _id: req.params.product_id
        }, function(err, product) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

module.exports = router;
